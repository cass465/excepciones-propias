package excepcionesString;

public class LongitudPassword extends Exception{

	private static final long serialVersionUID = 1L;

	public LongitudPassword(String mensaje) {
		super(mensaje);
	}
}
