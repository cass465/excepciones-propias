package excepcionesString;

public class Prefijo extends Exception {

	private static final long serialVersionUID = 1L;

	public Prefijo(String mensaje) {
		super(mensaje);
	}
}
