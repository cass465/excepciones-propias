package excepcionesString;

public class Verbo extends Exception {

	private static final long serialVersionUID = 1L;

	public Verbo(String mensaje) {
		super(mensaje);
	}
}
