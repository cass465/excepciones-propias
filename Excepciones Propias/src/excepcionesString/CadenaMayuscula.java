package excepcionesString;

public class CadenaMayuscula extends Exception {
	
	private static final long serialVersionUID = 1L;

	public CadenaMayuscula(String mensaje) {
		super(mensaje);
	}
}
