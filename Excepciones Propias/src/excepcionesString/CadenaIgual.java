package excepcionesString;

public class CadenaIgual extends Exception {

	private static final long serialVersionUID = 1L;

	public CadenaIgual(String mensaje) {
		super(mensaje);
	}
}
