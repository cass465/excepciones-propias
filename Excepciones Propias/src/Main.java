import excepcionesString.LongitudPassword;
import excepcionesString.Prefijo;
import excepcionesString.Verbo;
import excepcionesString.CadenaIgual;
import excepcionesString.CadenaMayuscula;
import excepcionesMath.Randomico;
import excepcionesMath.Potenciacion;
import excepcionesMath.RaizCuadrada;
import excepcionesMath.RedondeoMayor;
import excepcionesMath.RedondeoMenor;


import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws LongitudPassword {
		// TODO Auto-generated method stub
		Scanner leer = new Scanner(System.in);
		byte opcion;	
		String cadena, cadena2;
		double numero,potencia;
		
		do {			
			boolean existe=true;
			System.out.println("INGRESE UNA OPCION");
			System.out.println("1.  EXCEPCION 1");
			System.out.println("2.  EXCEPCION 2");
			System.out.println("3.  EXCEPCION 3");
			System.out.println("4.  EXCEPCION 4");
			System.out.println("5.  EXCEPCION 5");
			System.out.println("6.  EXCEPCION 6");
			System.out.println("7.  EXCEPCION 7");
			System.out.println("8.  EXCEPCION 8");
			System.out.println("9.  EXCEPCION 9");
			System.out.println("10. EXCEPCION 10");
			System.out.println("0.  SALIR");
			opcion = leer.nextByte();
			switch (opcion) {
			case 1:
				System.out.println("INGRESE UNA CONTRASEŅA");
				cadena = leer.nextLine();
				cadena = leer.nextLine();
				try {
					validarPassword(cadena);
				} catch (LongitudPassword e) {
					System.out.println(e.getMessage());

				}
				break;
			case 2:
				System.out.println("INGRESE UNA FRASE QUE COMIENCE POR 'L'");
				cadena = leer.nextLine();
				cadena = leer.nextLine();
				try {
					frasePorL(cadena);
				} catch (Prefijo e) {
					System.out.println(e.getMessage());

				}
				break;
			case 3:
				System.out.println("INGRESE UN VERBO");
				cadena = leer.nextLine();
				cadena = leer.nextLine();
				try {
					validarVerbo(cadena);
				} catch (Verbo e) {
					System.out.println(e.getMessage());
				}
				break;
			case 4:
				System.out.println("INGRESE UNA CADENA EN MAYUSCULAS");
				cadena = leer.nextLine();
				cadena = leer.nextLine();
				try {
					mayusculas(cadena);
				} catch (CadenaMayuscula e) {
					System.out.println(e.getMessage());
				}
				break;
			case 5:
				System.out.println("INGRESE DOS CADENAS IGUALES");
				System.out.print("CADENA 1:");
				cadena = leer.nextLine();
				cadena = leer.nextLine();
				System.out.print("CADENA 2:");
				cadena2 = leer.nextLine();
				try {
					cadenas(cadena, cadena2);
				} catch (CadenaIgual e) {
					System.out.println(e.getMessage());
				}
				break;
			case 6:
				System.out.println("INGRESE UN NUMERO PARA HALLAR SU RAIZ");
				numero=leer.nextDouble();
				try {
					raiz(numero);
				}catch(RaizCuadrada e) {
					System.out.println(e.getMessage());
					existe=false;
				}
				if(existe==true) {
					System.out.println("LA RAIZ DEL NUMERO ES:"+ Math.sqrt(numero));
				}
				
				break;
			case 7:
				System.out.println("INGRESE EL NUMERO");
				numero=leer.nextDouble();
				System.out.println("INGRESE EL VALOR DE LA POTENCIA");
				potencia=leer.nextDouble();
				try {
					potencia(numero,potencia);
				}catch(Potenciacion e){
					System.out.println(e.getMessage());
					existe=false;
				}
				if(existe==true) {
					System.out.println("EL RESULTADO ES:"+ Math.pow(numero,potencia));
				}
				
				break;
			case 8:
				 numero=Math.random();
				 
				 try {
					 aleatorio(numero);
				 }catch(Randomico e) {
					 System.out.println(e.getMessage());
					 existe=false;
						
				 }
				 if(existe==true) {
						System.out.println("EL NUMERO ESTA EN EL RANGO, NUMERO:"+ numero);
					}
				break;
			case 9:
				System.out.println("DIGITE UN NUMERO QUE PUEDA SER REDONDEADO A SU SIGUIENTE ENTERO POSITIVO");
				numero=leer.nextFloat();
				try {
					redondeoMayor(numero);
				}catch(RedondeoMayor e) {
					System.out.println(e.getMessage());
					existe=false;
				}
				if(existe==true) {
					System.out.println("EL REDONDEO DEL NUMERO ES:"+ Math.round(numero));
				}
				
				break;
			case 10:
				System.out.println("DIGITE UN NUMERO QUE PUEDA SER REDONDEADO A SU  ANTERIOR ENTERO POSITIVO");
				numero=leer.nextFloat();
				try {
					redondeoMenor(numero);
				}catch(RedondeoMenor e) {
					System.out.println(e.getMessage());
					existe=false;
				}
				if(existe==true) {
					System.out.println("EL REDONDEO DEL NUMERO ES:"+ Math.round(numero));
				}
				break;
			case 0:
				System.out.println("--EXCEPCIONES FINALIZADAS--");
				break;
			default:
				System.out.println("--OPCION INVALIDA--");
			}
		} while (opcion != 0);
	}

	private static void validarPassword(String cadena) throws LongitudPassword {
		if (cadena.length() < 8) {
			throw new LongitudPassword("SU CONTRASEŅA NECESITA MINIMO 8 CARACTERES");
		}

	}

	private static void frasePorL(String cadena) throws Prefijo {
		if (!cadena.startsWith("L")) {
			throw new Prefijo("LA FRASE INGRESADA NO COMIENZA CON 'L'");
		}

	}

	private static void validarVerbo(String cadena) throws Verbo {
		if (!cadena.endsWith("ar") && !cadena.endsWith("er") && !cadena.endsWith("ir") && !cadena.endsWith("AR")
				&& !cadena.endsWith("ER") && !cadena.endsWith("IR")) {
			throw new Verbo("LA CADENA NO CORRESPONDE A UN VERBO");
		}

	}

	private static void mayusculas(String cadena) throws CadenaMayuscula {
		if (!cadena.toUpperCase().equals(cadena)) {
			throw new CadenaMayuscula("LA CADENA INGRESADA NO ESTA EN MAYUSCULA");
		}
	}

	private static void cadenas(String cadena, String cadena2) throws CadenaIgual {
		if (!cadena.equals(cadena2)) {
			throw new CadenaIgual("LAS CADENAS NO COINCIDEN");
		}
	}	
	
	private static void raiz(double numero) throws RaizCuadrada{
		if (Math.sqrt(numero)!=(double)Math.sqrt(numero)) {
			throw new RaizCuadrada("ESTA RAIZ NO EXISTE");
		}		
	}
	
	private static void aleatorio (double numero) throws Randomico{
		if ( numero<0.3 || numero>0.7) {
			throw new Randomico(" NUMERO ALEATORIO FUERA DE RANGO");
			
		}
	}
	
	private static void potencia (double numero, double potencia) throws Potenciacion {
		if (Math.pow(numero ,potencia)<1 && numero!=1) {
			throw new Potenciacion ("ERROR, POTENCIA NEGATIVA");
			
		}
		
	}
	
	private static void redondeoMayor(double numero) throws RedondeoMayor {
		if (Math.round(numero)!=Math.ceil(numero) ) {
			throw new RedondeoMayor(" ERROR, ESTE NUMERO NO SE PUEDE REDONDEAR AL SIGUIENTE ENTERO POSITIVO");
		}
	}
	
	private static void redondeoMenor(double numero) throws RedondeoMenor {
		if (Math.round(numero)!=Math.floor(numero) ) {
			throw new RedondeoMenor (" ERROR, ESTE NUMERO NO SE PUEDE REDONDEAR AL SIGUIENTE ENTERO POSITIVO");
		}
	}
	
	
	
}
