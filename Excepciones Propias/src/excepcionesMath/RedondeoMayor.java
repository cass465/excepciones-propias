package excepcionesMath;

public class RedondeoMayor extends Exception {
	private static final long serialVersionUID = 1L;
	
	public RedondeoMayor (String mensaje) {
		super(mensaje);
	}

}
