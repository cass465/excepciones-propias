package excepcionesMath;

public class RedondeoMenor extends Exception {
	private static final long serialVersionUID = 1L; 
	
	public RedondeoMenor (String mensaje) {
		super(mensaje);
	}

}
