package excepcionesMath;

public class RaizCuadrada extends Exception {	
	
	private static final long serialVersionUID = 1L;

	public RaizCuadrada(String mensaje) {
		super(mensaje);
	}

}
